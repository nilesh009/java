/* Product of digit  */

import java.util.*;

class Demo {

	int proDigit(int num) {

		if(num == 0)
			return 1;

		int temp=num%10;

		return temp * proDigit(num/10);
	}

	public static void main(String [] args) {

		Scanner sc=new Scanner(System.in);
		Demo obj=new Demo();

		System.out.print("Enter number : ");

		int num=sc.nextInt();

		int ret=obj.proDigit(num);
		
		System.out.println(ret);
	}
}
