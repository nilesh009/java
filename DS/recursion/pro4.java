/* Sum Of odd no.*/

import java.util.*;

class Demo {
	
	int sumOdd(int num) {

		if(num == 0)
			return 0;

		if(num%2!=0)
			return num + sumOdd(--num);

		return sumOdd(--num);
	}

	public static void main(String [] args) {

		Scanner sc=new Scanner(System.in);
		Demo obj=new Demo();

		System.out.print("Enter number : ");

		int num=sc.nextInt();

     		int ret=obj.sumOdd(num);
		
		System.out.println(ret);
	}
}
