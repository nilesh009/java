/* cheak wether it is strong num or not */

import java.util.*;

class Demo {
	static int rem=0;

	int fact(int num) {

		if (num==0)
			return 1;

		return num * fact(--num);
	}

	int strongNum(int num) {
		
		if(num == 0)
			return 0;
		
	       	int temp=num%10 ;
		int rem=fact(temp);
	
		return  rem + strongNum(num/10);
	}

	public static void main(String [] args) {

		Scanner sc=new Scanner(System.in);
		Demo obj=new Demo();

		System.out.print("Enter number : ");

		int num=sc.nextInt();

     		int ret=obj.strongNum(num);
		
		if(num==ret) {			
			System.out.println(num + " is Strong no.");
		}else{
			System.out.println(num + " is not a Strong no.");
		}
	}
}
