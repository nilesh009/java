/* Maximun digit from given no. */

import java.util.*;

class Demo {

	static int max=Integer.MIN_VALUE;
	
	int maxDigit(int num) {

		if(num == 0)
			return 1;

		int temp=num%10;

		if(temp >= max)
			max=temp;

		return maxDigit(num/10);
	}

	public static void main(String [] args) {

		Scanner sc=new Scanner(System.in);
		Demo obj=new Demo();

		System.out.print("Enter number : ");

		int num=sc.nextInt();

		obj.maxDigit(num);
		
		System.out.println(max);
	}
}
