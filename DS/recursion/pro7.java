/* cheak wether it is magic num or not */

import java.util.*;

class Demo {
	static int flag=0;

	int fun(int num) {

		if (num==0)
			return 0;

		int temp=num%10;

		return  temp + fun(num/10);
	}

	int magicNum(int num) {
		
		if(num < 10) {	
			if(num==1){
				flag++;
				return 1;
			}else{
				return -1;
			}
		}

		int sum=fun(num);
	
		return magicNum(sum);
	}

	public static void main(String [] args) {

		Scanner sc=new Scanner(System.in);
		Demo obj=new Demo();

		System.out.print("Enter number : ");

		int num=sc.nextInt();

     		obj.magicNum(num);
		
		if(flag==1) {			
			System.out.println(num + " is Magic no.");
		}else{
			System.out.println(num + " is not Magic no.");
		}
	}
}
