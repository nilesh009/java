/* factorial take i/p from user */

import java.util.*;

class Demo {

	int factorial(int num) {

		if(num == 1)
			return 1;

		return num * factorial(--num);
	}

	public static void main(String [] args) {

		Scanner sc=new Scanner(System.in);
		Demo obj=new Demo();

		int num=sc.nextInt();

		int ret=obj.factorial(num);
		
		System.out.println(ret);
	}
}
