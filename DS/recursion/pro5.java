/* cheak is given no. is a palindrome or not*/

import java.util.*;

class Demo {
	static int rem=0;

	int palindrome(int num) {
		
		if(num == 0)
			return num;

		int temp=num%10;
		
	       	rem =rem * 10 + temp;

		return palindrome(num/10);
	}

	public static void main(String [] args) {

		Scanner sc=new Scanner(System.in);
		Demo obj=new Demo();

		System.out.print("Enter number : ");

		int num=sc.nextInt();

     		obj.palindrome(num);
		
		if(num==rem) {			
			System.out.println("Number is palindrome.");
		}else{
			System.out.println("Number is not palindrome.");
		}
	}
}
