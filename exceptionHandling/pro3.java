// nullPointerException 

class demo {

	void m2() {

		System.out.println("In m2");
	}

	void m1() {

		System.out.println("In m1");
	}

	public static void main(String args[]) {

		System.out.println("Start main");
		
		demo obj=new demo();
		obj.m1();

		obj=null;
		obj.m2();
		
		System.out.println("End main");
	}
}
