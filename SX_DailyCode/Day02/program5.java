import java.util.*;

class demo {

	public static void main(String[] args ) {

		Scanner obj=new Scanner(System.in);

		System.out.print("Enter String : ");
		String ch=obj.nextLine();
		
		int count=0;

		for(int j=0;j<ch.length();j++) {

			if(ch.charAt(j) == 'a' || ch.charAt(j) == 'e' || ch.charAt(j) == 'i' || ch.charAt(j) == 'o' || ch.charAt(j) == 'u') {				count++;
			}
		}

		if(count > 0 )
		       System.out.println("String have contains vowels : " + count);
		else	
		       System.out.println("String Not have contains vowels");	
	}
}
