import java.util.*;

class demo {

	public static void main(String [] args) {

		Scanner obj=new Scanner(System.in);
		System.out.print("Enter Num:");

		int num=obj.nextInt();
		int store=num;

		int sum=0;

		while(num!=0) {
			
			int temp=num;
			int fact=1;
			temp=temp%10;
			num/=10;

			for(int i=1;i<=temp;i++) {
				fact*=i;	
			}
			sum+=fact;
		}

		if(sum==store)
			System.out.println(store + " is Strong number.");
		else
			System.out.println(store + " is not Strong number.");
		
	}
}
