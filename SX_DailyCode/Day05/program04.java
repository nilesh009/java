import java.util.*;

class demo {

	public static void main(String [] args) {

		Scanner obj=new Scanner(System.in);
		System.out.print("Enter Row:");
		int row=obj.nextInt();

		int num1=1;
		int num2=2;

		for(int i=1;i<=row;i++) {
		 
			for(int j=1;j<=row;j++) {
				if(i%2!=0){
					System.out.print(num1 + " ");
					num1+=2;
				}else{
					System.out.print(num2 + " ");
					num2+=2;	
				}
			}
		System.out.println();
		}

	}
}
