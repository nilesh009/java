import java.util.*;

class Demo {
    public static void main(String[] args) {
        Scanner obj = new Scanner(System.in);

	System.out.print("Enter Row=");
	int row = obj.nextInt();

	int num=1;

        for (int i = 1; i <= row; i++) {
           
            for (int j = 1; j <= i; j++) {
                System.out.print(num++ +" ");
            }
	    num--;
            System.out.println();
        }
    }
}

