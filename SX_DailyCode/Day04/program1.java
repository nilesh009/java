import java.util.*;

class demo{
	public static void main(String [] args) {

		Scanner obj=new Scanner(System.in);

		System.out.println("Enter the No. of rows");
		int row=obj.nextInt();

		for(int i=1;i<=row;i++) {
		int ch=65;
			for(int j=1;j<=row;j++) {
				if(i%2!=0) {
					System.out.print((char)ch +" ");
					ch++;
				}else{
					System.out.print("# ");
				}

			}
			System.out.println();
		}
	}
}
