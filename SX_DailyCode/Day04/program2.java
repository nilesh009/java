import java.util.*;

class demo{
	public static void main(String [] args) {

		Scanner obj=new Scanner(System.in);

		System.out.println("Enter the No. of rows");
		int row=obj.nextInt();
		int ch1=65;
		for(int i=1;i<=row;i++) {
		int ch2=ch1;
			for(int j=1;j<=i;j++) {	
				System.out.print((char)ch2 +" ");
				ch2++;
			}
			System.out.println();
			ch1++;
		}
	}
}
