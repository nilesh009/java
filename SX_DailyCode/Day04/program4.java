import java.util.*;

class demo{
	public static void main(String [] args) {

		Scanner obj=new Scanner(System.in);

		System.out.print("Enter the Start:");
		int start=obj.nextInt();
		System.out.print("Enter the End:");
		int end=obj.nextInt();
		
		int sumOfDigit=0;

		for(int i=start;i<=end;i++) {
			int x=i;
			while(x!=0) {
				
				sumOfDigit+=x%10;
				x/=10;
		
			}
		}
		System.out.println("sum:" + sumOfDigit);
	}
}
