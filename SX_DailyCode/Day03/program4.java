import java.util.*;

class Day {

	public static void main(String []args) {
	Scanner obj=new Scanner(System.in);

	System.out.print("Enter the start: ");
	int start=obj.nextInt();
	
	System.out.print("Enter the end: ");
	int end=obj.nextInt();
	
	for(int i=start;i<=end;i++) {
		int num=i;
		int rev=0;
		while(num!=0) {
			int temp1=num%10;
			rev=rev * 10 + temp1;
			num/=10;
		}
		System.out.println(rev + "");
	}
	}
}
