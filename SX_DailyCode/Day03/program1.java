import java.util.*;

class Day {

	public static void main(String []args) {
	Scanner obj=new Scanner(System.in);

	System.out.print("Enter the row: ");
	int row=obj.nextInt();
	int ch=65;

	for(int i=1;i<=row;i++) {
		int ch1=ch;	
		for(int j=1;j<=row;j++) {
			if(i%2==1) {
				System.out.print( (char)ch1++ + " ");				
			}else{
				System.out.print( (char)ch1-- + " ");				
			}
		}
		if(i%2==1)
			ch=--ch1;
		else
			ch=++ch1;


		System.out.println();
	
	}
	}
}
