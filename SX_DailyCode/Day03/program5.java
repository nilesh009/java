import java.util.*;

class Day {

	public static void main(String []args) {
	Scanner obj=new Scanner(System.in);
	
	System.out.print("Enter the String: ");
	String str=obj.nextLine();

	int count=0;

	for(int i=0;i<str.length();i++) {
		char ch=str.charAt(i) ;
		if(!(ch >= 'A' && ch <= 'Z') && !(ch >= 'a' && ch <= 'z')) { 
			count++;
		}
	}

	if(count>0) {
		System.out.println("String have contain the other characters");
	}else{
		System.out.println("String contains only letters");
	}
	}
}
