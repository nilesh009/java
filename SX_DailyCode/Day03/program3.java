import java.util.*;

class Day {

	public static void main(String []args) {
	Scanner obj=new Scanner(System.in);

	System.out.print("Enter the num: ");
	int num=obj.nextInt();
	
		int temp=0;
		int store=num;

		while(num!=0) {
			int temp1=num%10;
			temp=temp * 10 + temp1;
			num/=10;
		}

		if(temp==store)
			System.out.println(store + " is palindrome number ");
		else
			System.out.println(store + " is not palindrome number ");
	}
}
