import java.util.*;

class laptops implements Comparable{

	String modelName=null;
	double price=00.00;

	laptops(String modelName,double price) {

		this.modelName=modelName;
		this.price=price;
	}

	public int compareTo(Object obj) {

		return (modelName.compareTo(((laptops)obj).modelName));
	}

	public String toString() {

		return modelName;
	}
}

class TreeSetRT {

	public static void main(String[] args) {

		TreeSet ts=new TreeSet();

		ts.add(new laptops("Asus_tufA15",60000.00));
		ts.add(new laptops("Lenovo_Legion5",75000.00));
		ts.add(new laptops("Dell_G17",90000.00));
		ts.add(new laptops("MacBook_Air",150000.00));

		System.out.println(ts);
	}
}
