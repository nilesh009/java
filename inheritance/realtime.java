class factory {
	factory() {

		System.out.println("In factory constructor");
		System.out.println("FactoryName=Britania");
	}
	void products() {
		System.out.println("Product=Biscuit,Cake & Snacks");
	}
}
class shop extends factory {	
	String sName="soham super market";
	shop() {
		System.out.println("In shop constructor");
	}
}
class client {
	public static void main(String[] args) {

		shop obj1=new shop();
		System.out.println("ShopName="+ obj1.sName);

		obj1.products();
	}
}
