// 3) Yield (static method) 

class MyThread extends Thread {


	public  void run() {

		 System.out.println(Thread.currentThread().getName());
	 }
}
class ThreadDemo {

	public static void main(String [] args) {

		MyThread obj=new MyThread();

		obj.start();

		obj.yield();

		for(int i=0;i<10;i++) {

			System.out.println("In main");
		}
	}
}
