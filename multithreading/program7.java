class MyThreadGroup extends Thread {

	MyThreadGroup(ThreadGroup tg,String str){
		super(tg,str);
	}

	MyThreadGroup(String str){
		super(str);
	}

	public void run () {
		
		System.out.println(Thread.currentThread());
	}
}

class ThreadGroupDemo {

	public static void main (String[] ab) {

		MyThreadGroup pthreadGP=new MyThreadGroup("core2web");

//		ThreadGroup cthreadGP=new ThreadGroup(pthreadGP,"INCHILD");

		MyThreadGroup obj1=new MyThreadGroup(pthreadGP,"C");
		MyThreadGroup obj2=new MyThreadGroup(pthreadGP,"java");
		MyThreadGroup obj3=new MyThreadGroup(pthreadGP,"python");

		obj1.start();
		obj2.start();
		obj3.start();
		
	}
}
