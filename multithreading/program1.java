// 1) Sleep concurrency method 

class MyThread extends Thread {

	public void run() {

		System.out.println(Thread.currentThread());		//Thread[thread-0,5,main]
	}
}
class ThreadDemo {


	public static void main(String[] args)throws InterruptedException {

		System.out.println(Thread.currentThread());  		// Thread[main,5,main]

		MyThread obj=new MyThread();
		obj.start();

	//	Thread.sleep(200);

		Thread.currentThread().setName("Core2web");		// to change the thread name
		System.out.println(Thread.currentThread());		//Thread[core2web,5,main]

	}
		
}

