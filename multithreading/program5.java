// ThreadGroup 

class MyThreadGroup extends Thread {

	public void run() {

		System.out.println(Thread.currentThread().getThreadGroup());
	}
}

class ThreadGroupDemo {

	public static void main(String[] srgs) {
	
		MyThreadGroup obj=new MyThreadGroup();

		obj.start();

		System.out.println(obj.getName());
	}
}
