//thread Groupe

class MyThreadGroup extends Thread {

	MyThreadGroup(String str) {
		super(str);
	}

	MyThreadGroup() {

	}

	public void run () {
		System.out.println(getName());
	//	System.out.println(Thread.currentThread().getThreadGroup());
	}
}

class ThreadGroupDemo {

	public static void main(String[] ab) {

		MyThreadGroup obj1=new MyThreadGroup("T1");
		obj1.start();

		MyThreadGroup obj2=new MyThreadGroup("T2");
		obj2.start();

		MyThreadGroup obj3=new MyThreadGroup();
		obj3.start();
		
		MyThreadGroup obj4=new MyThreadGroup();
		obj4.start();
		
		MyThreadGroup obj5=new MyThreadGroup("T3");
		obj5.start();
		
		MyThreadGroup obj6=new MyThreadGroup();
		obj6.start();
	}
}
