/* 10 
   I  H
   7  6  5 
   D  C  B  A */

import java.util.Scanner;

class nested {

	public static void main(String[] args) {
		
		Scanner myObj = new Scanner(System.in);

		System.out.print("Enter the No. of rows:");
		int row=myObj.nextInt();

		int n=row*(row+1)/2;
		int ch=n+64;
		int j=1;

		for(int i=1;i<=row;i++) {

			if(j<=i) {
				if(i%2==1) {
					System.out.print(n+" ");
				}else{
					System.out.print((char) ch+" ");
				}
				j++;
				ch--;
				n--;
				i--;
			}else{
				j=1;
				System.out.println();
			}
		}
	}
}
