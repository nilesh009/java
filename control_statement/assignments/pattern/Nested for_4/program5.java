/* A B C D 
   B C D
   C D
   D		*/

class demo {

	public static void main(String[] args) {

		int row=4;
		int j=1;
		int ch=65;
		int x=ch;

		for(int i=1;i<=row;i++ ) {

			if(j<=row-i+1) {

				System.out.print((char) x+" ");
				j++;
				i--;
				x++;
			}else{
				j=1;
				ch++;
				x=ch;
				System.out.println();
			}
		}
	}
}
