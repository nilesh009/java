/* 1 
   8   9
   27  16  125
   64  25  216 49 */

import java.util.Scanner;

class nested {

	public static void main( String[] args) {

		Scanner myObj= new Scanner (System.in);

		System.out.print("Enter the No. of rows:");

		int rows=myObj.nextInt();

		int x=1;
		int n=x;
		int j=1;

		for(int i=1;i<=rows;i++) {

			if(j<=i) {
				
				if(j%2==1) {
					System.out.print(n*n*n + " ");
				}else{
					System.out.print(n*n + " ");
				}
				n++;
				i--;
				j++;
			}else{
				x++;
				n=x;
				j=1;
				System.out.println();
			}
		}
	}
}
