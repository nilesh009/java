/*10 
  10 9
  9 8 7
  7 6 5 4 */

class demo {

	public static void main(String[] args) {

	int row=4;
	int j=1,n=row*(row+1)/2;

	for(int i=1;i<=row;i++ ) {

		if(j<=i) {

			System.out.print(n+" ");
			j++;
			i--;
			n--;
		}else{
			j=1;
			n++;
			System.out.println();
		}
	}
	}
}
