/* 3C 3C 3C 3C
   3C 3C 3C
   3C 3C 
   3C           */

class nested3 {

	public static void main(String[] args) {

		int row=3;
		int j=1;

		for(int i=1;i<=row;i++) {
			char ch='C';	
			int n=3;
			if(j<=row-i+1) {
				System.out.print(n+""+ch+" ");
				j++;
				i--;
			}else{
				j=1;
				System.out.println();
			}
		}
	}
}
