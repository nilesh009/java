/* 1 
 * 8  27
 * 64 125  216 */

class nested3 {

	public static void main(String[] args) {

		int row=3;
		int j=1;
		int n=1;

		for(int i=1;i<=row;i++) {

			if(j<=i) {
				System.out.print(n*n*n +"  ");
				j++;
				i--;
				n++;
			}else{
				j=1;
				System.out.println();
			}
		}
	}
}
