/* 9
   9 8
   9 8 7
   9 8 7 6 */

class nested3 {

	public static void main(String[] args) {

		int row=4;
		int j=1;
		int n=9;

		for(int i=1;i<=row;i++) {

			if(j<=i) {
				System.out.print(n+" ");
				j++;
				i--;
				n--;
			}else{
				n=9;
				j=1;
				System.out.println();
			}
		}
	}
}
