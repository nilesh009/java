/* 10 10 10 10 
   11 11 11
   12 12
   13           */

class nested3 {

	public static void main(String[] args) {

		int row=4;
		int j=1;
		int n=10;
		for(int i=1;i<=row;i++) {

			if(j<=row-i+1) {
				System.out.print(n+" ");
				j++;
				i--;
			}else{
				n++;
				j=1;
				System.out.println();
			}
		}
	}
}
