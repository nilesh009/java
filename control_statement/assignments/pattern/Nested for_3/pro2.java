/* 1 
   2 3
   4 5 6 */

class nested3 {

	public static void main(String[] args) {

		int row=4;
		int j=1;
		int n=1;

		for(int i=1;i<=row;i++) {

			if(j<=i) {
				System.out.print(n++ +" ");
				j++;
				i--;
			}else{
				j=1;
				System.out.println();
			}
		}
	}
}
