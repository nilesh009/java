/* 10
 * 9 8 
 * 7 6 5
 * 4 3 2 1 */
 

class nested3 {

	public static void main(String[] args) {

		int row=4;
		int j=1;
		int n=row*(row+1)/2; 

		for(int i=1;i<=row;i++) {

			if(j<=i) {
				System.out.print(n+" ");
				j++;
				i--;
				n--;
			}else{
				j=1;
				System.out.println();
			}
		}
	}
}
