/* J 
   I H
   G F E
   D C B A */

class nested3 {

	public static void main(String[] args) {

		int row=6;
		int j=1;
		int ch1=row*(row+1)/2;
		int ch2=ch1+64;

		for(int i=1;i<=row;i++) {

			if(j<=i) {
				System.out.print((char) ch2-- +" ");
				j++;
				i--;
			}else{
				j=1;
				System.out.println();
			}
		}
	}
}
