/* $ 
   @ @
   & & &
   # # # #
   $ $ $ $ $ 
   @ @ @ @ @ @
   & & & & & & &
   # # # # # # # # */

import java.io.*;

class demo {

	public static void main(String[] args)throws IOException {

		BufferedReader br=new BufferedReader( new InputStreamReader (System.in));

		System.out.println("Enter the No. of rows ");

		int rows=Integer.parseInt(br.readLine());

		int ch1=1;
		int ch2=1;

		for(int i=1;i<=rows;i++) {

			for(int j=1;j<=i;j++) {

				if(ch1==1) {

					System.out.print("$ ");

				}else if(ch1==2) {
					System.out.print("@ ");

				}else if(ch1==3) {
					System.out.print("& ");

				}else{
					System.out.print("# ");

				}
			}
			if(ch1==4) {

				ch1=0;
			}
			ch1++;

			System.out.println("");
		}

	}
}
