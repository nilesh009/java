/* WAP to take as a input and print the addition of Factorials of each digit from that no. */

import java.io.*;

class demo {

	public static void main(String[] ars) throws IOException {

		BufferedReader br=new BufferedReader (new InputStreamReader (System.in) );

		System.out.print("Enter the digit:");

		int digit=Integer.parseInt(br.readLine());

		int add=0;
		while(digit !=0) {

			int x=digit%10;
			digit=digit/10;

			int fact=1;

			for(int i=1;i<=x;i++) {

				fact=fact*i;
			}
			add=add+fact;
		}
		
		System.out.println("Addition of Factorial =" + add);
	}
}
