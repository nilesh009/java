/* 5 4 3 2 1 
   8 6 4 2 
   9 6 3
   8 4 
   5              */
import java.io.*;

class demo {

	public static void main(String[] args)throws IOException {

		BufferedReader br=new BufferedReader( new InputStreamReader (System.in));

		System.out.println("Enter the No. of rows ");

		int rows=Integer.parseInt(br.readLine());

		int x=rows;
		int j=1;
		for(int i=1;i<=rows;i++) {

			if(j<=rows-i+1) {
				
				System.out.print(x*i +" ");
				x=x-1;
				j++;
				i--;
			}else{
			j=1;
			x=rows-i;
			System.out.println();
			}
		}
	}
}

