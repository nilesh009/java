/* Print the series of prime no. gorm range. */

import java.io.*;

class demo {

	public static void main(String[] ars) throws IOException {

		BufferedReader br=new BufferedReader (new InputStreamReader (System.in) );

		System.out.print("Enter the Start:");

		int start=Integer.parseInt(br.readLine());

		System.out.print("Enter the End:");

		int end=Integer.parseInt(br.readLine());

		int count=0;


		for(int i=start;i<=end;i++) {
		

			for(int j=1;j<=i;j++) {
			
				if(i%j==0) {
					count++;
				}
			}
			if(count==2) {

				System.out.print (i +" ");
			}
			count=0;
		}
		System.out.println();	

	}
}
