/*0 
  1 1
  2 3 5
  8 13 21 34 */

import java.io.*;

class demo {

	public static void main(String[] ars) throws IOException {

		BufferedReader br=new BufferedReader (new InputStreamReader (System.in) );

		System.out.print("Enter the rows:");

		int rows=Integer.parseInt(br.readLine());
		
		int j=1;
		int temp1=0;
		int temp2=1;

		for(int i=1;i<=rows;i++) {

			if(j<=i) {
				
				System.out.print(temp1 +" ");
				int next=temp1 + temp2;
			       temp1=temp2;
		       	       temp2=next;	       		
				i--;
				j++;
			}else{

				j=1;
				System.out.println();
			}
		}
	}
}
