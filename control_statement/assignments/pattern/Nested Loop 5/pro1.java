
/* D3 C3 B2 A1
   A1 B2 C3 D4
   D4 C3 B2 A1
   A1 B2 C3 D4 */

import java.io.*;

class demo {

	public static void main(String[] args)throws IOException {

		BufferedReader br=new BufferedReader( new InputStreamReader (System.in));

		System.out.println("Enter the No. of rows ");

		int rows=Integer.parseInt(br.readLine());

		int ch1=68;
		int ch2=4;

		for(int i=1;i<=rows;i++) {

			for(int j=1;j<=rows;j++) {

				if(i%2==1) {
					if(ch1==69 && ch2==5) {

						ch1=68;
						ch2=4;
					}			
					System.out.print( (char) ch1--  +""+ ch2-- + " ");
				
				}else{
					if(ch1==64 && ch2==0 ) {

						ch1=65;
						ch2=1;
					}
					System.out.print( (char) ch1++  +""+ ch2++ + " ");
					
				}
			}
			System.out.println();
		}	
	}
}
