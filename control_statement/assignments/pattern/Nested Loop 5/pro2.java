/* # = = = =
   = # = = =
   = = # = =
   = = = # =
   = = = = # */


import java.io.*;

class demo {

	public static void main(String[] args)throws IOException {

		BufferedReader br=new BufferedReader( new InputStreamReader (System.in));

		System.out.println("Enter the No. of rows ");

		int rows=Integer.parseInt(br.readLine());

		int j=1;

		for(int i=1;i<=rows;i++) {
		
			if(j<=rows) {

				if(i==j) {
					System.out.print("# ");
				}else{
					System.out.print("= ");
				}

				i--;
				j++;
			}else{
				System.out.println();
				j=1;
			}
		}
	}
}

