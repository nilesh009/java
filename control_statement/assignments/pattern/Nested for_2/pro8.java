/* A b C d
   E f G h
   I j K l
   M n O p */

class logic {

	public static void main(String arr[]) {
               
		char ch1='a';
		char ch2='A';

		for(int i=1;i<=4;i++ ) {

			for(int j=1;j<=4;j++) {

				if(j%2==0) {
					System.out.print(ch1 +" ");
				}else{
					System.out.print(ch2 +" ");
				}
				ch1++;
				ch2++;
			}
			System.out.println();
		}
	}
}
