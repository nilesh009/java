class StringDemo {

	public static void main(String[] args) {

		String str1="Nilesh";

		String str2="Nilesh";
	
		String str3=new String ("Nilesh");

		String str4="Pranav";

		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str2));
		System.out.println(System.identityHashCode(str3));
		System.out.println(System.identityHashCode(str4));

	}
}
