/* method 4: StringBuffer reverse() */

class StringDemo {

	void myreverse(StringBuffer sb) {

		char arr[]=new char[sb.length()];

		int temp=sb.length()-1;

		for(int i=0;i<sb.length();i++) {
		
			arr[i]=sb.charAt(temp);
			temp--;
		}		

		for(int x:arr) {
			System.out.print((char)x);
		}


	}

	public static void main(String[] args ) {

		StringDemo obj=new StringDemo();

		StringBuffer sb=new StringBuffer("Nilesh");

		obj.myreverse(sb);

		System.out.println("");
	}
}
