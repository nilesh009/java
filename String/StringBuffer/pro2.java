/* StringBuffer delete(int start,int end); */

class StringDemo {

	void mydelete(int start,int end,StringBuffer sb) {
	
		char arr[]=new char[(sb.length()+start)-(end+1)];

		int count=0;
		for(int i=0;i<start;i++) {
			
			arr[i]=sb.charAt(i);
			count++;
		}

		for(int i=end+1;i<sb.length();i++) {
			arr[count]=sb.charAt(i);
			count++;
		}

		for(int x:arr) {

			System.out.print((char)x);
		}
	System.out.println("");
	}
	public static void main(String args[] ) {

		StringDemo obj=new StringDemo();

		StringBuffer sb=new StringBuffer("NileshRajuSartape");         //Enter String

		obj.mydelete(6,9,sb);					       //Enter start and End Index

		System.out.println("");
	}
}
