/* StringBuffer apend() */

class StringDemo {

	void myappend(StringBuffer sb1,StringBuffer sb2) {

		char arr[]=new char[sb1.length()+sb2.length()];

		int count=0;

		for(int i=0;i<sb1.length();i++) {
			arr[i]=sb1.charAt(i);
			count++;
		}
		int j=0;
		for(int i=count;i<sb1.length()+sb2.length();i++) {
			arr[count]=sb2.charAt(j);
			j++;
			count++;
		}

		for(int x:arr) {
			System.out.print((char)x);
		}


	}

	public static void main(String[] args ) {

		StringDemo obj=new StringDemo();

		StringBuffer sb=new StringBuffer("Nilesh");
		StringBuffer sb2=new StringBuffer("Sartape");
	
		obj.myappend(sb,sb2);

		System.out.println("");
	}
}
