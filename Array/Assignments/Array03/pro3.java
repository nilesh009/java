/* Find  composite no. from array and return index*/

import java.util.Scanner;

class ArrayDemo {


	public static void main (String args[]) {

		Scanner sc=new Scanner(System.in);

		System.out.print("Enter size of array:");
		int size=sc.nextInt();

		System.out.println("Enter Array Elements:");
		int arr[]=new int[size];

		for(int j=0;j<arr.length;j++) {

			arr[j]=sc.nextInt();
		}

		for(int i=0;i<arr.length;i++) {

			int count=0;

			for(int j=1;j*j<=arr[i];j++) {

				if(arr[i]%j==0) {
					count++;
				}
			}
			if(count>=2) {

				System.out.println("composite element found at "+ i +" Index");
			}
		}
		
	}
}
