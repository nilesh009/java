/* Armstrong number in array */

import java.util.Scanner;

class ArrayDemo {


	public static void main (String args[]) {

		Scanner sc=new Scanner(System.in);

		System.out.print("Enter size of array:");
		int size=sc.nextInt();

		System.out.println("Enter Array Elements:");
		int arr[]=new int[size];

		for(int j=0;j<arr.length;j++) {

			arr[j]=sc.nextInt();
		}

		for(int i=0;i<arr.length;i++) {
			
			int temp1=arr[i];
			int count=0;
			int add=0;

			while(temp1!=0) {

				temp1=temp1/10;
				count++;
			}
			temp1=arr[i];

			while(temp1!=0) {

				int rem=temp1%10;
				int mult=1;

				for(int k=1;k<=count;k++) {

					mult=mult*rem;
				}
				add=add+mult;
				temp1=temp1/10;
			}

			if(add==arr[i]) {

				System.out.println("Armstrong Number found "+ arr[i] + " at Index "+ i );
			}
		}

	}
}
