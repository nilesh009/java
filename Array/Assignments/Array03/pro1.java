/* WAP count of digit in elements of array */

import java.util.*;

class arrayDemo {
void fun(int arr[]) {

	for(int i=0;i<arr.length;i++) {
		int temp=arr[i];
		int count=0;

		while(temp!=0) {

			temp=temp/10;
			count++;
		}
		arr[i]=count;		
	}
}

	public static void main(String[] args) {

		Scanner sc=new Scanner(System.in);

		arrayDemo obj=new arrayDemo();

		System.out.print("Enter size of Array:");
		int size=sc.nextInt();
	
		System.out.println("Enter Array Elements:");
		int arr1[]=new int[size];

		for(int i=0;i<arr1.length;i++) {

			arr1[i]=sc.nextInt();
		}

		obj.fun(arr1);

		System.out.println("Digit count:");
		for(int x:arr1) {

			System.out.println(x);
		}
	}
}
