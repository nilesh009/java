/* Find palindrome number and return index */

import java.io.*;

class ArrayDemo {


	public static void main (String args[])throws IOException {

		BufferedReader br=new BufferedReader (new InputStreamReader (System.in));

		System.out.print("Enter size of array:");
		int size=Integer.parseInt(br.readLine());

		System.out.println("Enter Array Elements:");
		int arr[]=new int[size];

		for(int j=0;j<arr.length;j++) {

			arr[j]=Integer.parseInt(br.readLine());
		}

		for(int i=0;i<arr.length;i++) {
			
			int x=arr[i];
			int temp=0;

			while(x!=0) {
				int rem=x%10;
				temp=temp*10+rem;
				x=x/10;
			}
			if(arr[i]==temp) {
				System.out.println("Index:"+ i);
			}
		}

	}
}
