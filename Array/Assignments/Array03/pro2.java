/* reverse digit */

import java.util.Scanner;

class ArrayDemo {

	static void fun(int xarr[]) {

		System.out.println("Reverse Digit:");
		for(int x=0;x<xarr.length;x++) {
						
			int temp1=xarr[x];
			int temp2=0;

			while(temp1!=0) {
				int temp3=temp1%10;
				temp2=temp2*10+temp3;
				temp1=temp1/10;
			}
			System.out.println(temp2);
		}
	}

	public static void main (String args[]) {

	//	arrayDemo obj=new arrayDemo();

		Scanner sc=new Scanner(System.in);

		System.out.print("Enter size of array:");
		int size=sc.nextInt();

		System.out.println("Enter Array Elements:");
		int arr[]=new int[size];

		for(int j=0;j<arr.length;j++) {

			arr[j]=sc.nextInt();
		}

		fun(arr);
	}
}
