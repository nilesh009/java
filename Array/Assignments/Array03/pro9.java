/* Second max element in array */

import java.io.*;

class ArrayDemo {


	public static void main (String args[])throws IOException {

		BufferedReader br=new BufferedReader (new InputStreamReader(System.in));

		System.out.print("Enter size of array:");
		int size=Integer.parseInt(br.readLine());

		System.out.println("Enter Array Elements:");
		int arr[]=new int[size];

		for(int j=0;j<arr.length;j++) {

			arr[j]=Integer.parseInt(br.readLine());
		}
	
		int max=arr[0];
		int Smax=0;

		for(int i=0;i<arr.length;i++) {
		
			for(int j=i+1;j<arr.length;j++) {
				if(arr[i]>arr[j]) {
					int temp=arr[i];
					arr[i]=arr[j];
					arr[j]=temp;
				}		
			}
		}
		System.out.println("Second Maximum element in Array is : "+ arr[arr.length-2]);
		
	}
}
