/* Uncommon elements between two array */

import java.util.Scanner;

class arraydemo {


	public static void main(String[] args) {

		Scanner sc=new Scanner(System.in);
		
		int count=0;

		System.out.print("Enter Size:");
		int size=sc.nextInt();

		System.out.println("Enter Elements of Array1:");
		int arr1[]=new int[size];

		for(int k=0;k<arr1.length;k++) {
			arr1[k]=sc.nextInt();
		}

		System.out.println("Enter Elements of Array2:");
		int arr2[]=new int[size];
	
		for(int s=0;s<arr2.length;s++) {
			arr2[s]=sc.nextInt();
		}

		System.out.println("Uncommon Elements:");
		for(int i=0;i<arr1.length;i++) {
			for(int j=0;j<arr2.length;j++) {
				
				if(arr1[i]==arr2[j]) {
					count++;
					break;							
				}
			}
			if(count==0) {
				System.out.print(arr1[i]+ " " + '\n');
			}else{
				count=0;
			}
		}

		System.out.println();
		int count1=0;

		for(int i=0;i<arr1.length;i++) {
			for(int j=0;j<arr2.length;j++) {
				
				if(arr2[i]==arr1[j]) {
					count1++;
					break;							
				}
			}
			if(count1==0) {
				System.out.print(arr2[i]+ " ");
			}else{
				count1=0;
			}
		}
		System.out.println();
	}
}
