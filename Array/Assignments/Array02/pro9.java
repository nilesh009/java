/* merge 2 array */

import java.util.Scanner;

class arraydemo {


	public static void main(String[] args) {

		Scanner sc=new Scanner(System.in);

		System.out.print("Enter Size:");
		int size=sc.nextInt();

		System.out.println("Enter Elements of Array1:");
		int arr1[]=new int[size];
		int arr3[]=new int [size*2];
		int arr2[]=new int[size];

		for(int i=0;i<arr1.length;i++) {
			arr1[i]=sc.nextInt();
			arr3[i]=arr1[i];
		}

		System.out.println("Enter Elements of Array2:");
		
		for(int i=0;i<arr2.length;i++) {
			arr2[i]=sc.nextInt();
			arr3[i+size]=arr2[i];
		}
		
		System.out.println("Merge Array:");
		for(int i=0;i<size*2;i++) {

			System.out.print("|"+ arr3[i] +"|");
		}	

		System.out.println();
	}
}
