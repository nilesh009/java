//// maximum element from array 

import java.util.Scanner;

class arraydemo {


	public static void main(String[] args) {

		Scanner sc=new Scanner(System.in);

		System.out.print("Enter Size:");
		int size=sc.nextInt();

		System.out.println("Enter Elements:");
		int arr[]=new int[size];

		for(int i=0;i<arr.length;i++) {

			arr[i]=sc.nextInt();
		}

		int max=arr[0];

		for(int i=0;i<arr.length;i++) {
		
			if(max <arr[i]) {

				max=arr[i];
			}	
		}
		System.out.println("Maximum Element in Array:"+ max) ;
	}
}
