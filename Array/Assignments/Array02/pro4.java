// to find element and return index ;

import java.util.Scanner;

class arraydemo {


	public static void main(String[] args) {

		int count=0;


		Scanner sc=new Scanner(System.in);

		System.out.print("Enter Size:");
		int size=sc.nextInt();

		System.out.println("Enter Elements:");
		int arr[]=new int[size];

		for(int i=0;i<arr.length;i++) {
			arr[i]=sc.nextInt();			
		}

		System.out.print("Enter Elements to search:");
		int search=sc.nextInt();
	
		for(int i=0;i<arr.length;i++) {
			if(arr[i]==search) {

				System.out.println("Found At index:"+ i);
				count++;
			}		
		}
		
		if(count<=0)
			System.out.println(search +"is not available in given array" );
	}
}
