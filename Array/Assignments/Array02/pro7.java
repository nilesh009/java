/* common elements between two array */

import java.util.Scanner;

class arraydemo {


	public static void main(String[] args) {

		Scanner sc=new Scanner(System.in);

		System.out.print("Enter Size:");
		int size=sc.nextInt();

		System.out.println("Enter Elements of Array1:");
		int arr1[]=new int[size];

		for(int i=0;i<arr1.length;i++) {
			arr1[i]=sc.nextInt();
		}

		System.out.println("Enter Elements of Array2:");
		int arr2[]=new int[size];
	
		for(int i=0;i<arr2.length;i++) {
			arr2[i]=sc.nextInt();
		}

		System.out.println("Same Elements:");
		for(int k=0;k<arr1.length;k++) {
		
			for(int j=0;j<arr2.length;j++) {
		
				if(arr1[k]==arr2[j]) {

					System.out.print(arr1[k]+ " " + '\n');

				}
			}
		}
	}
}
