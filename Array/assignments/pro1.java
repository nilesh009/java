/* odd sum */

import java.util.Scanner;

class arraydemo {


	public static void main(String[] args) {

		int sum=0;

		Scanner sc=new Scanner(System.in);

		System.out.print("Enter Size:");
		int size=sc.nextInt();

		System.out.println("Enter Elements:");
		int arr[]=new int[size];

		for(int i=0;i<arr.length;i++) {

			arr[i]=sc.nextInt();

			if(arr[i]%2!=0) {

				sum+=arr[i];
			}
		}
		System.out.println("Sum of odd element:"+sum);
	}
}
