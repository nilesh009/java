/* divisible by 5 */

import java.util.Scanner;

class arraydemo {


	public static void main(String[] args) {

		Scanner sc=new Scanner(System.in);

		System.out.print("Enter Size:");
		int size=sc.nextInt();

		System.out.println("Enter Elements:");
		int arr[]=new int[size];

		for(int i=0;i<arr.length;i++) {

			arr[i]=sc.nextInt();
		}

		System.out.println("Elements Divisible by 5 are :");
		for(int i=0;i<arr.length;i++) {	
		
			if(arr[i]%5==0) {
				System.out.println(arr[i] + " ");
			}
		}
	}
}
