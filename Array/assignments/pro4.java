/* print only vowels */

import java.util.Scanner;

class arraydemo {


	public static void main(String[] args) {

		Scanner sc=new Scanner(System.in);

		System.out.print("Enter Size:");
		int size=sc.nextInt();

		System.out.println("Enter Elements:");
		char arr[]=new char[size];

		for(int i=0;i<arr.length;i++) {

			arr[i]=sc.next().charAt(0);
		}

		System.out.println("Output: ");

		for(int i=0;i<arr.length;i++) {
			if(arr[i]=='a' || arr[i]=='e'|| arr[i]=='i' || arr[i]=='o' || arr[i]=='u' ) {

				System.out.println(arr[i] + " ");
			}
					
		}
	}
}
