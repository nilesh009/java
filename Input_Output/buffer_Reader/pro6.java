/* String Tokenizer (this class is use to break the string in tokens  )*/

import java.io.*;
import java.util.*;

class demo {

	public static void main(String[] args)throws IOException {

		BufferedReader br=new BufferedReader (new InputStreamReader (System.in));

		System.out.println("Enter Apartment,Wing,FlatNo");

		String info=br.readLine();

		StringTokenizer obj=new StringTokenizer(info," ");

		String token1=obj.nextToken();
		String token2=obj.nextToken();
		String token3=obj.nextToken();

		System.out.println("Apartment="+token1);
		System.out.println("Wing="+token2);
		System.out.println("FlatNo="+token3);
	}
}
