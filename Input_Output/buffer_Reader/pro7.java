/* Real time Example of StringTokenizer class */

import java.util.*;
import java.io.*;

class demo {

	public static void main(String[] args)throws IOException {

		BufferedReader br=new BufferedReader (new InputStreamReader (System.in));
		
		System.out.println("Enter the Details of your profile ");

		System.out.println("pName,pRank,coins,Level");

		String Details=br.readLine();

		StringTokenizer obj= new StringTokenizer(Details," ");

		String token1=obj.nextToken();
		String token2=obj.nextToken();
		String token3=obj.nextToken();
		String token4=obj.nextToken();

		System.out.println("Player Name="+token1);
		System.out.println("Player Rank="+token2);
		System.out.println("Total coins ="+token3);
		System.out.println("Level="+token4);
	}
}
