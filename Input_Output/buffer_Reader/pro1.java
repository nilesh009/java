/* ISR: isr only reads single character */

import java.io.*;

class demo {

	public static void main( String[] args)throws IOException{

		InputStreamReader isr=new InputStreamReader(System.in);

		BufferedReader br=new BufferedReader (isr);

		System.out.print("Enter Your name:");

		String name=br.readLine();

		System.out.print("Enter Your Age:");

		int age=Integer.parseInt(br.readLine());

		System.out.println("Name="+name);
		System.out.println("Age="+age);

	}
}
