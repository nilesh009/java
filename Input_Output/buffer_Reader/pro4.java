/*  	 \n problem in bufferdReader */

import java.io.*;
import java.util.*;

class demo {

	public static void main(String[] args )throws IOException {

		BufferedReader br=new BufferedReader (new InputStreamReader (System.in));

		System.out.print("Enter the Apartment Name:");

		String name=br.readLine();

		System.out.print("Enter the Apartment wing:");

		char wing=(char) br.read();
		
		br.skip(1);
		System.out.println("Enter Flat No:");

		int flat=Integer.parseInt(br.readLine());

		System.out.println("Name="+name);
		System.out.println("Wing="+wing);
		System.out.println("Flat="+flat);
	}
}
