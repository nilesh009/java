/* String Tokenizer Methods */

import java.util.*;

class Demo {

	public static void main(String[] args ) {

		Scanner obj =new Scanner (System.in);

		System.out.println("Enter Your Details");

		String str1=obj.nextLine();

		System.out.println(str1);

		StringTokenizer st=new StringTokenizer(str1," ");
		
		System.out.println("Count of Token: "+ st.countTokens());

		int i=1;
		while(st.hasMoreTokens()) {

			System.out.println(i++ +": "+ st.nextToken());
		}
	}
}
